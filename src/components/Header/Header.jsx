import './header.css';

const Header = () => {
    return (
        <header className="header">
            <p className="header__logo">
            Gallery
            </p>
        </header>
    )
}

export default Header;