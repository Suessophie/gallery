import { useSelector, useDispatch } from 'react-redux';
import {
    setAlbum,
    filterPhoto,
    resetFilterPhoto,
    resetPagination
} from '../../actions';
import './filter.css';

const Filter = () => {
    const {album, albums} = useSelector(state => state);
    const dispatch = useDispatch();

    const handleFilter = e => {
        const value = +e.target.value;
        dispatch(setAlbum(value));
    
        value ? dispatch(filterPhoto(value)) : dispatch(resetFilterPhoto());
        dispatch(resetPagination());
      }

    return (
        <div className="action">
            <h6 className="action__title">
                Filter by:
            </h6>
            <select onChange={handleFilter}
                value={album}
                name="album">
                <option value="">
                    All albums
                </option>
                {albums.map(album => (
                    <option key={album}
                    value={album}>
                    Album №{album}
                    </option>
                ))}
            </select>
        </div>
    )
}

export default Filter;