import { useSelector, useDispatch } from 'react-redux';
import deleteIcon from '../../images/delete.png';
import {
    setZoomPhoto,
    showModal,
    deletePhoto
} from '../../actions';
import './photos.css';

const Photos = () => {
    const {currentPhotos} = useSelector(state => state);

    const dispatch = useDispatch();

    const handleShowModal = photo => {
        dispatch(setZoomPhoto(photo));
        dispatch(showModal());
      }

    const handleDeletePhoto = id => dispatch(deletePhoto(id));

    return (
        <div className="list">
            {currentPhotos.map(photo => (
                <div key={photo.id}
                    className="list__item">
                    <div className="photo">
                    <img className="photo__img"
                        src={photo.thumbnailUrl}
                        alt={photo.title}
                        onClick={() => handleShowModal(photo)}/>
                        <h2 className="photo__title">
                            {photo.title}
                        </h2>
                        <h3 className="photo__album">
                            Album №{photo.albumId}
                        </h3>
                        <button className="photo__delete"
                            onClick={() => handleDeletePhoto(photo.id)}>
                            <img src={deleteIcon}
                                width="24"
                                alt="Delete"/>
                        </button>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default Photos;