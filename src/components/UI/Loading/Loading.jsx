import spinner from '../../../images/spinner.gif';
import './loading.css';

const Loading = () => {
    return (
        <div className="loading">
            <img className="loading__spinner"
                src={spinner}
                alt="Loading"/>
        </div>
    )
}

export default Loading;