import { useSelector, useDispatch } from 'react-redux';
import {hideModal} from '../../../actions';

import './modal.css';

const Modal = () => {
    const {zoomPhoto} = useSelector(state => state);
    const dispatch = useDispatch();

    const handleHideModal = () => dispatch(hideModal());

    return (
        <div className="modal"
            onClick={handleHideModal}>
            <div className="modal__content"
                onClick={(e) => e.stopPropagation()}>
                <div className="modal__header">
                    <h2 className="modal__title">
                        {zoomPhoto.title}
                    </h2>
                    <button className="modal__close"
                        onClick={handleHideModal}>
                        &times;
                    </button>
                </div>
                <div className="modal__body">
                    <img className="modal__img"
                        src={zoomPhoto.url}
                        alt="{zoomPhoto.title}"/>
                </div>
            </div>
        </div>
    )
}

export default Modal;