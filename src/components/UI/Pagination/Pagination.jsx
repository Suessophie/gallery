import { useSelector, useDispatch } from 'react-redux';
import cn from 'classnames';
import {
    clickPagination
} from '../../../actions';
import './pagination.css';

const Pagination = () => {
    const {currentPage, pages} = useSelector(state => state);
    const dispatch = useDispatch();

    const handleClickPagination = numberOfPage => {
        dispatch(clickPagination(numberOfPage));
    }

    return (
        <div className="pagination">
            <button className="pagination__link"
            onClick={() => handleClickPagination(currentPage - 1)}
            disabled={currentPage === 1}>
                &#8810;
            </button>
            {pages.map(page => (
            <button key={page}
                className={cn('pagination__link', {'pagination__link--active': page === currentPage})}
                onClick={() => handleClickPagination(page)}>
                {page}
            </button>
            ))}
            <button className="pagination__link"
            onClick={() => handleClickPagination(currentPage + 1)}
            disabled={currentPage === pages.length}>
                &#8811;
            </button>
        </div>
    )
}

export default Pagination;