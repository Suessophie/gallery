const initialState = {
    initialPhotos: [],
    photos: [],
    currentPhotos: [],
    modalVisible: false,
    currentPage: 1,
    photosPerPage: 20,
    album: '',
    zoomPhoto: null,
    albums: [],
    pages: []
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "SHOW_MODAL":
            return {
                ...state,
                modalVisible: true
            };
        case "HIDE_MODAL":
            return {
                ...state,
                modalVisible: false
            };
        case "DELETE_PHOTO":
            const initialPhotosDeletePhoto = state.initialPhotos.filter(photo => photo.id !== action.payload);
            const photosDeletePhoto = state.photos.filter(photo => photo.id !== action.payload);
            let currentPhotosDeletePhoto = photosDeletePhoto.slice((state.currentPage - 1) * state.photosPerPage, state.currentPage * state.photosPerPage);
            let currentPageDeletePhoto;
            if (!currentPhotosDeletePhoto.length) {
                currentPhotosDeletePhoto = photosDeletePhoto.slice((state.currentPage - 2) * state.photosPerPage, (state.currentPage - 1) * state.photosPerPage);
                currentPageDeletePhoto = state.currentPage - 1;
            }

            return {
                ...state,
                initialPhotos: initialPhotosDeletePhoto,
                photos: photosDeletePhoto,
                currentPhotos: currentPhotosDeletePhoto,
                currentPage: currentPageDeletePhoto ? currentPageDeletePhoto : state.currentPage
            };
        case "FILTER_PHOTO":
            const photosFilterPhoto = state.initialPhotos.filter(photo => photo.albumId === action.payload);
            const currentPhotosFilterPhoto = photosFilterPhoto.slice(0, state.photosPerPage);

            return {
                ...state,
                photos: photosFilterPhoto,
                currentPhotos: currentPhotosFilterPhoto
            };
        case "RESET_FILTER_PHOTO":
            const photosResetFilterPhoto = [...state.initialPhotos];
            const currentPhotosResetFilterPhoto = photosResetFilterPhoto.slice(0, state.photosPerPage);

            return {
                ...state,
                photos: photosResetFilterPhoto,
                currentPhotos: currentPhotosResetFilterPhoto
            };
        case "CLICK_PAGINATION":
            const currentPhotosClickPagination = state.photos.slice((action.payload - 1) * state.photosPerPage, action.payload * state.photosPerPage);

            return {
                ...state,
                currentPage: action.payload,
                currentPhotos: currentPhotosClickPagination
            };
        case "RESET_PAGINATION":
            return {
                ...state,
                currentPage: 1
            };
        case "SET_ALBUM":
            return {
                ...state,
                album: action.payload,
            };
        case "FETCH_PHOTOS":
            const currentPhotosFetchPhotos = state.photos.slice(0, state.photosPerPage);
            let allAlbums = [];
            for (let i = 0; i < action.payload.length; i++) {
                allAlbums.push(action.payload[i].albumId);
            }
            allAlbums = [...new Set(allAlbums)];
            const pagesFetchPhotos = [];
            for (let i = 1; i <= Math.ceil(action.payload.length / state.photosPerPage); i++) {
                pagesFetchPhotos.push(i);
            }

            return {
                ...state,
                initialPhotos: action.payload,
                photos: action.payload,
                currentPhotos: currentPhotosFetchPhotos,
                albums: allAlbums,
                pages: pagesFetchPhotos
            };
        case 'SET_ZOOM_PHOTO':
            return {
                ...state,
                zoomPhoto: {...action.payload}
            };
        default:
            return state;
    }
}

export default reducer;