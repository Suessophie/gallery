export const showModal = () => ({type: 'SHOW_MODAL'});

export const hideModal = () => ({type: 'HIDE_MODAL'});

export const deletePhoto = id => ({type: 'DELETE_PHOTO', payload: id});

export const filterPhoto = value => ({type: 'FILTER_PHOTO', payload: value});

export const resetFilterPhoto = () => ({type: 'RESET_FILTER_PHOTO'});

export const clickPagination = numberOfPage => ({type: 'CLICK_PAGINATION', payload: numberOfPage});

export const resetPagination = () => ({type: 'RESET_PAGINATION'});

export const setAlbum = value => ({type: 'SET_ALBUM', payload: value});

export const fetchPhotos = () => {
    return async dispatch => {
        const response = await fetch('http://jsonplaceholder.typicode.com/photos');
        const json = await response.json();
        dispatch({type: 'FETCH_PHOTOS', payload: json});
    }
}

export const setZoomPhoto = photo => ({type: 'SET_ZOOM_PHOTO', payload: photo});

export const setCurrentPhotos = () => ({type: 'SET_CURRENT_PHOTOS'});