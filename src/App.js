import {useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {fetchPhotos} from './actions';

import Header from './components/Header/Header';
import Photos from './components/Photos/Photos';
import Filter from './components/Filter/Filter';
import Pagination from './components/UI/Pagination/Pagination';
import Modal from './components/UI/Modal/Modal';
import Loading from './components/UI/Loading/Loading';

function App() {
  const {modalVisible, currentPhotos} = useSelector(state => state);

  const dispatch = useDispatch();

  useEffect(function () {
    dispatch(fetchPhotos());
  }, [dispatch])

  return (
    <>
      <Header/>
      <div className="container">
        {currentPhotos.length ? (
          <>
            <Filter/>
            <Photos/>
            <Pagination/>
          </>
        ) : (
          <Loading/>
        )}
      </div>
      {modalVisible && (
        <Modal/>
      )}
    </>
  );
}

export default App;